import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Product } from '../product/product';
import { Observable, throwError } from 'rxjs';
import {tap,catchError} from 'rxjs/operators';
import { WeekDay } from '@angular/common';

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) 
  { }
  path = "http://localhost:3000/products";
   
  getProducts(categoryId:number): Observable<Product[]> { 
   
   let url = this.path;
   if(categoryId){
     url =this.path+ "?categoryId="+ categoryId 
   }  

   const result =this.http.get<Product[]>(url).pipe( 
   
    tap(data=>console.log(JSON.stringify(data))), 
    catchError(this.handleError) 
  );
    return  result
    }

    addProduct(product:Product): Observable<Product> {
      const httpOptions={
        headers: new HttpHeaders({
          'Content-Type':'application/json',
          'Authorization':'Token'
        })
      }
      return this.http.post<Product>(this.path, product, httpOptions).pipe(
        tap(data=>console.log(JSON.stringify(data))),
        catchError(this.handleError)
      )
    }

  handleError(err: HttpErrorResponse) {
    let ErrorMesagge = ''
    if (err.error instanceof ErrorEvent) {; 
    ErrorMesagge = "Bir hata oluştu"+ err.error.message
    }
    else {
      ErrorMesagge = 'Sistemsel bir hata'
     }
     return throwError(ErrorMesagge);
  }
}

